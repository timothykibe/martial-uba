import {
    Platform
  } from 'react-native';
  import React from "react";
  import { Root } from "native-base";
  import {
    StackNavigator
  } from 'react-navigation';
  //import { Root } from "native-base";
  import Pin from './src/pages/pin_entry.js';
  import Stages from './src/pages/stages/stages';
  import Riders from './src/pages/Riders/riders.js';
  import Rider from './src/pages/Rider/rider.js';
  import Home from './src/pages/home.js';

    /*
  import Tasks from './src/pages/Tasks/tasks.js';
  import Notifications from './src/pages/Notifications/notifications.js';
  import Issues from './src/pages/Issues/issues.js';
  import Riders from './src/pages/Riders/riders.js';
  import Report from './src/pages/Report/report.js';
  import ChatList from './src/pages/Chat/chatlist.js';
  import Chat from './src/pages/Chat/chat.js';
  //import LoginScreen from './src/pages/Auth/login';
  import SignUp from './src/pages/Auth/signup';
  
  */
  const Routes = {
    Rider: { screen: Rider },
    Riders: { screen: Riders },
    Stages: { screen: Stages },
    Home: { screen: Home },
    Pin: { screen: Pin }

    /*
    
    Chat: { screen: Chat },
    ChatList: { screen: ChatList },
    
    
    Tasks: { screen: Tasks },
    Notifications: { screen: Notifications },
   
    Issues: {screen: Issues},
    Report: {screen: Report}
    */
  };
  
  export default AppNavigator = StackNavigator(
    {
      ...Routes,
    },
    {
      headerMode: "none",
      initialRouteName: 'Pin'
    }
  );
  
  
  