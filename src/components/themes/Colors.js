const colors = {
  appColor: '#000000',
  black: '#000000', 
  light_gray: '#D1CECE',
  dark_gray: '#A5A3A3',
  white: '#FDFDFD', 
  transparent: 'rgba(0,0,0,0)', 
};

export default colors;