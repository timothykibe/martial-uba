import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  Animated,
  Image,
  Dimensions,
  TouchableOpacity,
  StatusBar,
  Platform
} from "react-native";
import ImageSlider from 'react-native-image-slider';
import Communications from 'react-native-communications';
import MapView from "react-native-maps";
import { Markers } from "../data/Markers";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import styles from "./styles";
import Ratings from "../components/common/Ratings";
import { View, Container,Icon, Content, Button, Left, Right,  Picker, Item, Grid, Col, Toast, Text as NBText } from 'native-base';
import Marker from "../components/common/Marker";
import Card from "../components/map/Card";
import Iconn from "../components/common/Iconn";
import ModalFilter from '../components/modal/ModalFilter';
import { Colors, AppStyles, Metrics } from "../themes/index";
const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = height / 4;
const CARD_WIDTH = width - 50;
export default class screens extends Component {

  state = {
    openIndex: null,
    render: false,
    show: false,
    overlayImage: false,
    coords: {
      left: new Animated.Value(0),
      top: new Animated.Value(0),
      width: new Animated.Value(0),
      height: new Animated.Value(0),
    },
    transition: {},
    markers:[],
   initial_latitude: {},
    initial_longitude: {} ,
    region: {
      //To implement location of the first profile 
      latitude: -1.228631,
      longitude: 36.805057,
      latitudeDelta: 0.01864195044303443,
      longitudeDelta: 0.010142817690068,
    },
   
    modalVisible: false

  };

  componentWillMount() {
    this.fetchMarkers();
    this.index = 0;
    this.images = {};
    this.animation = new Animated.Value(0);
    this.opacityAnimation = new Animated.Value(0);
  }
  
///Function to fetch server api data

fetchMarkers() {
  return fetch('http://mapping.porterlogics.ltd/profiles-api')
    .then((response) => response.json())
    .then((responseData) => {
   // return responseJson.markers;
     // console.log("The good" + responseJson)
     this.setState({ markers: responseData.markers});
     this.setState({ initial_latitude: responseData.markers[0].latitude});
     this.setState({ initial_longitude: responseData.markers[0].longitude});
     console.log("My results are:  ",this.state.markers);
     console.log("My results areeeeeeee:  ",this.state.initial_latitude);
    
     markers: responseData.markers.map(function(marker){

     // this.setState({latlng: marker.coordinate});
                // const profile_location = hit._source.markers;           
      return { 
        latlng: {
          latitude: marker.latitude,
          longitude: marker.longitude
          }

        }
        
       
      })
     
    })
    .catch((error) => {
      console.error(error);
    });
}

  
 componentDidMount() {

   //this.fetchMarkers();
   console.log("My results are:  ",this.state.markers);
   console.log("returnedl", this.state.latlng);
    // We should detect when scrolling has stopped then animate
    // We should just debounce the event listener here


    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
      if (index >= this.state.markers.length) {
        index = this.state.markers.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }

      clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index;
          
          const { latitude } = this.state.markers[index];
          const { longitude } = this.state.markers[index];

          this.map.animateToRegion(
            
            {

              latitude: latitude,
              longitude: longitude,
              latitudeDelta: this.state.region.latitudeDelta,
              longitudeDelta: this.state.region.longitudeDelta,

            },
            350
          );
        }
      }, 10);
    });
  }

  handleClose = () => {
    const { openIndex: index } = this.state;

    this.tImage.measure(
      (tframeX, tframeY, tframeWidth, tframeHeight, tpageX, tpageY) => {
        this.state.coords.top.setValue(tpageY);
        this.state.coords.left.setValue(tpageX);
        this.state.coords.width.setValue(tframeWidth);
        this.state.coords.height.setValue(tframeHeight);
        Animated.timing(this.opacityAnimation, {
          toValue: 0,
          duration: 100, // THIS SHOULD BE INTERPOLATION FROM X AND Y!
        }).start();

        this.setState(
          {
            overlayImage: true,
          },
          () => {
            this.images[
              index
            ].measure(
              (frameX, frameY, frameWidth, frameHeight, pageX, pageY) => {
                Animated.parallel([
                  Animated.timing(this.state.coords.top, {
                    toValue: pageY,
                    duration: 250,
                  }),
                  Animated.timing(this.state.coords.left, {
                    toValue: pageX,
                    duration: 250,
                  }),
                  Animated.timing(this.state.coords.width, {
                    toValue: frameWidth,
                    duration: 250,
                  }),
                  Animated.timing(this.state.coords.height, {
                    toValue: frameHeight,
                    duration: 250,
                  }),
                ]).start(() => {
                  this.setState({
                    overlayImage: false,
                    render: false,
                    openIndex: null,
                  });
                });
              }
              );
          }
        );
      }
    );
  };

  handleShow = index => {
    this.setState(
      {
        openIndex: index,
        render: true,
        transition: this.state.markers[index],
      },
      () => {
        this.images[
          index
        ].measure((frameX, frameY, frameWidth, frameHeight, pageX, pageY) => {
          this.state.coords.top.setValue(pageY);
          this.state.coords.left.setValue(pageX);
          this.state.coords.width.setValue(frameWidth);
          this.state.coords.height.setValue(frameHeight);
          this.setState(
            {
              overlayImage: true,
            },
            () => {
              this.tImage.measure(
                (
                  tframeX,
                  tframeY,
                  tframeWidth,
                  tframeHeight,
                  tpageX,
                  tpageY
                ) => {
                  Animated.parallel([
                    Animated.timing(this.state.coords.top, {
                      toValue: tpageY,
                      duration: 250,
                    }),
                    Animated.timing(this.state.coords.left, {
                      toValue: tpageX,
                      duration: 250,
                    }),
                    Animated.timing(this.state.coords.width, {
                      toValue: tframeWidth,
                      duration: 250,
                    }),
                    Animated.timing(this.state.coords.height, {
                      toValue: tframeHeight,
                      duration: 250,
                    }),
                  ]).start(() => {
                    this.opacityAnimation.setValue(1);
                    this.setState({
                      overlayImage: false,
                    });

                  });
                }
              );
            }
          );
        });
      }
    );
  };

  openModal = () => {
    this.setState({ modalVisible: true });
  }

  closeModal = () => {

    {this.fetchMarkers()};
    this.setState({ modalVisible: false });
  }

  renderImages() {
    let images = [];
    this.state.markers.map((img, i) => {
      images.push(
          <TouchableWithoutFeedback
            key={i}
            onPress={() => this.openGallery(i)}
          >

            <Image
              source={{uri:img}}
              style={{width: Dimensions.get('window').width, height: 350}}
              resizeMode="cover"
            />

          </TouchableWithoutFeedback>
      );
    });
    return images;
  }

  render() {
    const interpolations = this.state.markers.map((marker, index) => {
      const inputRange = [
        (index - 1) * CARD_WIDTH,
        index * CARD_WIDTH,
        (index + 1) * CARD_WIDTH + 1,
      ];
      const scale = this.animation.interpolate({
        inputRange,
        outputRange: [1, 2.5, 1],
        extrapolate: "clamp",
      });
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0.35, 1, 0.35],
        extrapolate: "clamp",
      });
      return { scale, opacity };
    });

    const { transition, coords, render, region, markers,initial_latitude,initial_longitude, modalVisible } = this.state;
    const lat = initial_latitude;
    const long =initial_longitude ;

    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
    
        <MapView
          ref={map => this.map = map}
          
          initialRegion={{
            //To implement location of the first profile 
            
            latitude: initial_latitude,
            longitude: initial_longitude,
            latitudeDelta: 0.01864195044303443,
            longitudeDelta: 0.010142817690068,
          }}
          style={styles.container}
        >
          {markers.map((marker, index) => {
            const scaleStyle = {
              transform: [
                {
                  scale: interpolations[index].scale,
                },
              ],
            };
            
            const opacityStyle = {
              opacity: interpolations[index].opacity,
            };
            return (
             // latitude: 37.4228775,
             // longitude:-122.085133
              <MapView.Marker
                key={index}
                  coordinate={{
                    latitude: marker.latitude,
                    longitude: marker.longitude,
                    }}
                    onPress={() => this.handleShow(index)}
              >
                <Marker
                  onPress={() => this.handleShow(index)}
                  image={marker.image_url_prefix+marker.primary_image}
                  color={"red"}
                
                />

              </MapView.Marker>
            );

          })}
        </MapView>

        <Animated.ScrollView
          horizontal
          scrollEventThrottle={16}
          showsHorizontalScrollIndicator={false}
          snapToInterval={CARD_WIDTH}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
          )}
          style={styles.scrollView}
          contentContainerStyle={styles.endPadding}
        >
          {markers.map((marker, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => this.handleShow(index)}
              
            >

              <Card
                index={index}
                images={this.images}
                marker={marker}
              />
            </TouchableOpacity>
          ))}


        </Animated.ScrollView>
        {this.state.overlayImage &&
          <Animated.Image
            resizeMode="cover"
            style={{
              position: "absolute",
              top: coords.top,
              left: coords.left,
              width: coords.width,
              height: coords.height,
            }}
            source={{ uri: transition.image }}
          />}
        {render &&
          <Animated.View
            style={[
              styles.transitionContainer,
              StyleSheet.absoluteFill,
              { opacity: this.opacityAnimation },
            ]}
          >
            <Iconn
              width={35}
              image={require('../resources/icons/close.png')}
              tintColor={Colors.black}
              styleIcon={{ marginTop: Platform.OS === 'ios' ? 50 : 0 }}
              onPress={this.handleClose}
            />

          
        <Content>

          <View style={{backgroundColor: '#fdfdfd', paddingTop: 10, paddingBottom: 10, paddingLeft: 12, paddingRight: 12, alignItems: 'center'}}>
          <Image
              source={{ uri: transition.image_url_prefix.concat(transition.primary_image) }}
              style={styles.transitionImage}
             style={{width: 100}}
              ref={tImage => this.tImage = tImage}
              resizeMode="contain"
            />
              <Image
              source={{ uri: transition.image_url_prefix.concat(transition.primary_image) }}
              style={{width: 100}}
            />
          <View style={{marginTop: 5,width: '100%' , padding: 10, borderWidth: 1, borderRadius: 3, borderColor: 'rgba(149, 165, 166, 0.3)'}}>
              <Text style={{marginBottom: 5}}>Description</Text>
             
              <View style={{width: 50, height: 1, backgroundColor: 'rgba(44, 62, 80, 0.5)', marginLeft: 7, marginBottom: 10}} />
              <View style={{flex: 1, flexDirection:'row',width: '100%'}}>
              <Image
              source={{ uri: transition.image_url_prefix.concat(transition.primary_image) }}
              style={[
                AppStyles.center,
                {
                  borderWidth: 1,
                  width: 60,
                  height: 60,
                  borderRadius: 60 / 2,
                  marginBottom: 8,
                  marginRight: 20,
                  borderColor: "#3a3a3a"
                }
              ]}
             // resizeMode={"cover"}
             // ref={img => images[index] = img}
            />
             
           
              <NBText style={{marginRight:20}} note>
                {transition.description}
              </NBText>
            
              <Ratings value={transition.rating} />
              </View>
            
            </View>

             <View style={{marginTop: 5,width: '100%', padding: 3, borderWidth: 1, borderRadius: 3, borderColor: 'rgba(149, 165, 166, 0.3)'}}>
              <Text style={{marginBottom: 5}}>Info</Text>
              <View style={{width: 50, height: 1, backgroundColor: 'rgba(44, 62, 80, 0.5)', marginLeft: 7, marginBottom: 10}} />
             <View style={{flex: 1, flexDirection:'row',width: '100%'}}>
              
              <Button block  icon transparent style={{backgroundColor: '#fdfdfd'}} 
              onPress={() => Communications.phonecall('0123456789', true)}>
                <Icon style={{color: Colors.navbarBackgroundColor}} name='ios-call' />
              </Button>
            
                <Button block  style={{backgroundColor: Colors.navbarBackgroundColor}}>
                  <Text style={{color: "#fdfdfd", marginLeft: 1}}>Get direction</Text>
                </Button>
                <Button block  icon transparent style={{backgroundColor: '#fdfdfd'}}>
                <Icon style={{color: Colors.navbarBackgroundColor}} name='ios-heart' />
              </Button>
              <Text style={{marginBottom: 5}}>Gigiri, Nairobi-Kenya</Text>
            </View>
             
           
            </View>
            <View style={{marginTop: 5,width: '100%', padding: 10, borderWidth: 1, borderRadius: 3, borderColor: 'rgba(149, 165, 166, 0.3)'}}>
              <Text style={{marginBottom: 5}}>Images</Text>
              <View style={{width: 50, height: 1, backgroundColor: 'rgba(44, 62, 80, 0.5)', marginLeft: 7, marginBottom: 10}} />
              <ImageSlider
             style={{height: 200, width:300}}
               images={ 
                [
                  'http://mapping.porterlogics.ltd/images/profile_multiple/11/11_1_1520501675.jpg',
                  'http://mapping.porterlogics.ltd/images/profile_multiple/8/8_1_1519480745.JPG',
                  'http://mapping.porterlogics.ltd/images/profile_multiple/11/11_1_1520501639.jpg',
                  'http://mapping.porterlogics.ltd/images/profile_multiple/11/11_1_1520501639.jpg'
                ]
               }
              />

            </View>
            <View style={{marginTop: 5,width: '100%', padding: 10, borderWidth: 1, borderRadius: 3, borderColor: 'rgba(149, 165, 166, 0.3)'}}>
              <Text style={{marginBottom: 5}}>tags/ads</Text>
              <View style={{width: 50, height: 1, backgroundColor: 'rgba(44, 62, 80, 0.5)', marginLeft: 7, marginBottom: 10}} />
            </View>
            

            <View style={{marginTop: 5,width: '100%', padding: 10, borderWidth: 1, borderRadius: 3, borderColor: 'rgba(149, 165, 166, 0.3)'}}>
              <Text style={{marginBottom: 5}}>Reviews</Text>
              <View style={{width: 50, height: 1, backgroundColor: 'rgba(44, 62, 80, 0.5)', marginLeft: 7, marginBottom: 10}} />
              <NBText note>
                No reviews
              </NBText>
            </View>

           
            
          </View>
         
        </Content>

          </Animated.View>
        }


        <Iconn
          width={45}
          backgroundColor={Colors.white}
          borderRadius={true}
          tintColor={Colors.black}
          image={require("../resources/icons/search.png")}
          styleIcon={[style.filter, AppStyles.shadow]}
          onPress={() => this.openModal()}
        />

        <ModalFilter
          visible={modalVisible}
          onCancel={this.closeModal}
        />
      </View>
    );


    
  }
}

const style = StyleSheet.create({
  filter: {
    position: "absolute",
    top: Platform.OS === 'ios' ? 30 : 0,
    right: 0,
    margin: Metrics.baseMargin
  },
  buttonContainer: {
    position: "absolute",
    marginTop:50,
    top: Platform.OS === 'ios' ? 30 : 0,
    left: 0,
    margin: Metrics.baseMargin
  },
})
