import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  
  Left,
  Right,
  Body
} from "native-base";
//import styles from "./styles";
import {  TouchableOpacity } from 'react-native';

export default class Riders extends Component {

  state = {
    datas: []
  };

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("http://niteout.porterlogics.ltd/profiles-api");
    const json = await response.json();
    this.setState({ datas: json.markers});
  };

  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container >
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Riders</Title>
          </Body>
          <Right>

          <Button transparent /*onPress={() => this.props.navigation.goBack()}*/>
              
            </Button>
          </Right>
        </Header>

        <Content>
          <List
            dataArray={this.state.datas}
            renderRow={dataa =>
               
              <ListItem avatar onPress={() =>
                navigate('Rider', { id: dataa.id})
              }>
                <Left>
            
                  <Thumbnail small source={{ uri: 'http://niteout.porterlogics.ltd/images/profile_multiple/15/15_1_1520598391.jpg' }}  />
                </Left>
             
                <Body>
                  <Text>
                    {dataa.title}
                  </Text>
                  <Text numberOfLines={1} note>
                    {"Rating: "+dataa.rating}
                  </Text>
                </Body>
              
                <Right>
                  <Text >
                    {dataa.ratting}
                  </Text>
                </Right>
              </ListItem>
           
              }
          />
          
        </Content>
      </Container>
    );
  }
}

//export default Rider;