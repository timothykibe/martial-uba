import React, { Component } from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
export default class Test extends Component {
  state = {
    data: []
  };

  componentWillMount() {
    this.fetchData();
  }
  fetchData = async () => {
    const response = await fetch("http://uba.porterlogics.ltd/profiles-api");
    const json = await response.json();
    this.setState({ data: json.markers});
  };

  render() {

    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.data}
          keyExtractor={(x, i) => i}
          renderItem={({ item }) =>
          
            <Text>
              {`${item.title} ${item.latitude} ${item.longitude}${item.ratting}`}
            </Text>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});