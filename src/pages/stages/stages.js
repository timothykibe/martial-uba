import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Right,
  Body
} from "native-base";
import styles from "./styles";
import {  TouchableOpacity } from 'react-native';

export default class Stages extends Component {

  state = {
    datas: []
  };

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("http://uba.porterlogics.ltd/profiles-api");
    const json = await response.json();
    this.setState({ datas: json.markers});
  };

  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Stages</Title>
          </Body>
          <Right>

          <Button transparent /*onPress={() => this.props.navigation.goBack()}*/>
              <Icon name="map" />
            </Button>
          </Right>
        </Header>

        <Content>
          <List
            dataArray={this.state.datas}
            renderRow={dataa =>
               
              <ListItem avatar onPress={() =>
                navigate('Riders', { id: dataa.id })
              }>
                <Left>
                  <Thumbnail small source={{ uri: 'https://png.icons8.com/metro/1600/9-circle.png' }}  />
                </Left>
               
                <Body>
                  <Text>
                    {dataa.title}
                  </Text>
                  <Text numberOfLines={1} note>
                    {"Rating: "+dataa.rating}
                  </Text>
                </Body>
               
                <Right>
                  <Text >
                    {dataa.ratting}
                  </Text>
                </Right>
              </ListItem>
           
              }
          />
          
        </Content>
      </Container>
    );
  }
}

//export default Issues;