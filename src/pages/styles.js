export default {
    container: {
      backgroundColor: '#ffffff',
      padding: 5,
      margin:10,
      borderRadius:3,
      shadowColor: '#000000',
      shadowOpacity: 0.3,
      shadowRadius:3,
      bgColor:'#ffffff',
      elevation:3,
      shadowOffsetWidth: 3,
      shadowOffsetHeight: 3
  },
    text: {
      alignSelf: "center",
      marginBottom: 7
    },
    mb: {
      marginBottom: 15
    }
  };
  