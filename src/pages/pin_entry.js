/**
 * Created by dungtran on 8/20/17.
 */
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Alert
} from 'react-native';

import CodeInput from 'react-native-confirmation-code-input';

export default class pin extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      code: ''
    };
  }
  
  _onFulfill(code) {
    // TODO: call API to check code here
    // If code does not match, clear input with: this.refs.codeInputRef1.clear()
    if (code == 'Q234E') {
      Alert.alert(
        'Confirmation Code',
        'Successful!',
        [{text: 'OK'}],
        { cancelable: false }
      );
    } else {
      Alert.alert(
        'Confirmation Code',
        'Code not match!',
        [{text: 'OK'}],
        { cancelable: false }
      );
      
      this.refs.codeInputRef1.clear();
    }
  }
  
  _onFinishCheckingCode1(isValid) {
    console.log(isValid);
    if (!isValid) {
      
     
      Alert.alert(
        'Confirmation Code',
        'Code not match!',
        [{text: 'OK'}],
        { cancelable: false }
      );
     
    } else {

      const { navigate } = this.props.navigation;
     navigate('Home')
       /*
      Alert.alert(
        'Confirmation Code',
        'Successful!',
        [{text: 'OK'}],
        { cancelable: false }
      );
      */
    }
  }
  
  _onFinishCheckingCode2(isValid, code) {
    console.log(isValid);
    if (!isValid) {
      Alert.alert(
        'Confirmation Code',
        'Code not match!',
        [{text: 'OK'}],
        { cancelable: false }
      );
    } else {
      this.setState({ code });
      const { navigate } = this.props.navigation;
      navigate('Home')
       /*
      Alert.alert(
        'Confirmation Code',
        'Successful!',
        [{text: 'OK'}],
        { cancelable: false }
      );
      */
    }
  }
  
  render() {
    
    return (
      <View style={styles.container}>
        <ScrollView >
          
          
          <View style={styles.inputWrapper3}>
            <Text style={styles.inputLabel3}>Enter pin / Scan finger print</Text>
            <CodeInput
              ref="codeInputRef2"
              secureTextEntry
              compareWithCode='12345'
              activeColor='#000000'
              inactiveColor='#FFFFFF'
              autoFocus={true}
              keyboardType={'numeric'}
              ignoreCase={true}
              inputPosition='center'
              size={50}
              onFulfill={(isValid) => this._onFinishCheckingCode1(isValid)}
              containerStyle={{ marginTop: 30 }}
              codeInputStyle={{ borderWidth: 1.5 }}
            />
          </View>
  
         
        </ScrollView> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4876FF'
  },
  titleWrapper: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  title: {
    color: 'red',
    fontSize: 16,
    fontWeight: '800',
    paddingVertical: 30
  },
  wrapper: {
    marginTop: 3
  },
  inputWrapper1: {
    paddingVertical: 50,
    paddingHorizontal: 20,
    backgroundColor: '#009C92'
  },
  inputWrapper2: {
    paddingVertical: 50,
    paddingHorizontal: 20,
    backgroundColor: '#E0F8F1'
  },
  inputWrapper3: {
    paddingVertical: 50,
    paddingHorizontal: 20,
    backgroundColor: '#4876FF'
  },
  inputLabel1: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '800'
  },
  inputLabel2: {
    color: '#31B404',
    fontSize: 14,
    fontWeight: '800',
    textAlign: 'center'
  },
  inputLabel3: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '800',
    textAlign: 'center'
  }
});

//AppRegistry.registerComponent('example', () => example);