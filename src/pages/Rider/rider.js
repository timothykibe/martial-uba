import React, { Component } from "react";
import { Platform,Image,View } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  ListItem,
  Text,
  Badge,
  Left,
  Right,
  Body,
  Switch,
  Radio,
  Picker,Card, CardItem, Thumbnail,
  Separator
} from "native-base";
import styles from "./styles";
//import Oval from "../../components/containers/Oval"
const Item = Picker.Item;

export default class Rider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedItem: undefined,
     
      selected1: "key1",
      results: {
        items: []
      }
    };
  }
  onValueChange(value= string) {
    this.setState({
      selected1: value
    });
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Profile</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'http://uba.porterlogics.ltd/images/rider_multiple/4/4_1_1523368003.png'}} />
                <Body>
                  <Text>Name: Angela Munene </Text>
                  <Text note>Ratting: 3.5</Text>
                </Body>
              </Left>

            </CardItem>
            <CardItem cardBody>
            <View style={{flex:4,flexDirection:"column",justifyContent:'space-between', margin:10}}>
            <View style={{flex: 1, flexDirection:"row",alignItems: 'flex-start'}}>
                    <Text style={{fontSize: 16,margin:5}}>
                    NAME:
                    </Text>
                    <Text note >
                    Angela Munene
                    </Text>
            </View>

            <View style={{flex: 1, flexDirection:"row",alignItems: 'flex-start'}}>
                    <Text style={{fontSize: 16,margin:5}}>
                    ID NO :
                    </Text>
                    <Text note >
                    33316691
                    </Text>
            </View>

            <View style={{flex: 1, flexDirection:"row",alignItems: 'flex-start'}}>
                    <Text style={{fontSize: 16,margin:5}}>
                    STAGE:
                    </Text>
                    <Text note >
                    Kencom stage-Nairobi county
                    </Text>
            </View>

            <View style={{flex: 1, flexDirection:"row",alignItems: 'flex-start'}}>
                    <Text style={{fontSize: 16,margin:5}}>
                    Email:
                    </Text>
                    <Text note >
                    angiemeshack@gmail.com
                    </Text>
            </View>

            <View style={{flex: 1, flexDirection:"row",alignItems: 'flex-start'}}>
                    <Text style={{fontSize: 16,margin:5}}>
                    PHONE:
                    </Text>
                    <Text note >
                    +254700350815
                    </Text>
            </View>

          
            </View>
            </CardItem>
            <CardItem>
              <Left>
              <Button
                  warning
                  onPress={() =>
                    alert("Would you like to edit")}
          >
            <Text>KCMB345C</Text>
          </Button>
              </Left>
              <Body>
              <Button
                 
                >
                  <Icon active name="chatbubbles" />
                  <Text>7 complaints</Text>
                </Button>

              </Body>
            </CardItem>
          </Card>

          <Separator bordered noTopBorder />
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#4CDA64" }}>
                <Icon name="arrow-dropdown" />
              </Button>
            </Left>
            <Body>
              <Text>Status</Text>
            </Body>
            <Right>
              <Picker
                note
                iosHeader="Select Your Sim"
                iosIcon={<Icon name="ios-arrow-down-outline" />}
                mode="dropdown"
                selectedValue={this.state.selected1}
                onValueChange={this.onValueChange.bind(this)}
              >
                <Item label="Disabled" value="key0" />
                <Item label="Active" value="key1" />
              </Picker>
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch" />
              </Button>
            </Left>
            <Body>
              <Text>2 Helmets</Text>
            </Body>
            <Right>
              <Switch value={true} onTintColor="#50B948" />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch" />
              </Button>
            </Left>
            <Body>
              <Text>2 Reflector Jackets</Text>
            </Body>
            <Right>
            <Switch value={true} onTintColor="#50B948" />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch" />
              </Button>
            </Left>
            <Body>
              <Text>Insurance</Text>
            </Body>
            <Right>
            <Switch value={true} onTintColor="#50B948" />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch"  />
              </Button>
            </Left>
            <Body>
              <Text>Driver's License</Text>
            </Body>
            <Right>
            <Switch value={false} onTintColor="#50B948"  onPress={() =>
                    alert("Would you like to edit")} />
            </Right>
          </ListItem>
          <ListItem icon last>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch" />
              </Button>
            </Left>
            <Body>
              <Text> First Aid Kit</Text>
            </Body>
            <Right>
            <Switch value={false} onTintColor="#50B948" />
            </Right>
          </ListItem>

  

          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch" />
              </Button>
            </Left>
            <Body>
              <Text> PSV Permit</Text>
            </Body>
            <Right>
            <Switch value={false} onTintColor="#50B948" />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch" />
              </Button>
            </Left>
            <Body>
              <Text>County Permit</Text>
            </Body>
            <Right>
            <Switch value={true} onTintColor="#50B948" />
            </Right>
          </ListItem>
          <ListItem icon >
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="switch" />
              </Button>
            </Left>
            <Body>
              <Text>Advanced Training</Text>
            </Body>
            <Right>
            <Switch value={true} onTintColor="#50B948" />
            </Right>
          </ListItem>
          <Separator bordered />
          
          <ListItem icon last>
            <Left>
              <Button style={{ backgroundColor: "#0000FF" }}>
                <Icon active name="notifications" />
              </Button>
            </Left>
            <Body>
              <Text>Complaints</Text>
            </Body>
            <Right>
              <Badge style={{ backgroundColor: "#FD3C2D" }}>
                <Text>2</Text>
              </Badge>
            </Right>
          </ListItem>
         
        </Content>
      </Container>
    );
  }
}

//export default NHListIcon;

