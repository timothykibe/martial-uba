import React, { Component } from 'react';
import { Container,Badge, Header, Content,Title,Right,Left,Icon,Button, Card, View, CardItem, Body, Text } from 'native-base';

import { CardViewWithIcon } from 'react-native-simple-card-view';
import{StyleSheet} from 'react-native';
export default class Home extends Component {
  state = {
     stagesno:null
  };

  componentWillMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("http://uba.porterlogics.ltd/profiles-api");
    const json = await response.json();
    //this.setState({ stagesno: json.markers});
   // var count = Object.keys(json).length;
    this.setState({stagesno: Object.keys(json.markers).length })
  };

  render() {
    return (
        <Container>
        <Header>
          <Body>
            <Title></Title>
          </Body>
          <Right>
  
            <Button transparent>
              <Icon name='md-qr-scanner'  />
            </Button>
          </Right>
        </Header>
        <View style={{flex:2,flexDirection:"row",justifyContent:'space-between'}}>
        <View style={{flex:1}}>
            <CardViewWithIcon
                style={{
                  backgroundColor: '#ffffff',
                  padding: 5,
                  margin:10,
                  borderRadius:3,
                  shadowColor: '#000000',
                  shadowOpacity: 0.3,
                  shadowRadius:3,
                  bgColor:'#ffffff',
                  elevation:3,
                  shadowOffsetWidth: 3,
                  shadowOffsetHeight: 3
                }}
                androidIcon={ 'md-book' }
                iosIcon={ 'ios-jet-outline' }
                iconBgColor={ '#4876FF' }
                onPress={() => this.props.navigation.navigate("Tasks")}
                iconColor={ '#FFFFFF' }
                title={ 'Tasks' }
                //content={'12'}
              />
            </View>
            <View style={{flex:1}}>
            <CardViewWithIcon
                style={{
                  backgroundColor: '#ffffff',
                  padding: 5,
                  margin:10,
                  borderRadius:3,
                  shadowColor: '#000000',
                  shadowOpacity: 0.3,
                  shadowRadius:3,
                  bgColor:'#ffffff',
                  elevation:3,
                  shadowOffsetWidth: 3,
                  shadowOffsetHeight: 3
                }}
                androidIcon={ 'md-notifications' }
                iosIcon={ 'ios-jet-outline' }
                iconBgColor={ '#4876FF' }
                onPress={() => this.props.navigation.navigate("Notifications")}
                iconColor={ '#FFFFFF' }
                title={ 'Notifications' }
              //  content={'4'}
              />
            </View>
        </View>

<View style={{flex:2,flexDirection:"row",justifyContent:'space-between'}}>
            <View style={{flex:1}}>
            <CardViewWithIcon
                style={{  
                  backgroundColor: '#ffffff',
                  padding: 5,
                  margin:10,
                  borderRadius:3,
                  shadowColor: '#000000',
                  shadowOpacity: 0.3,
                  shadowRadius:3,
                  bgColor:'#ffffff',
                  elevation:3,
                  shadowOffsetWidth: 3,
                  shadowOffsetHeight: 3
                }}
                androidIcon={ 'md-bicycle' }
                iconBgColor={ '#4876FF' }
                iconBgColor={ '#4876FF' }
                onPress={() => this.props.navigation.navigate("Stages")}
                iconColor={ '#FFFFFF' }
                title={ 'Stages' }
                content={this.state.stagesno}
               
              />

            </View>
            <View style={{flex:1}}>

            <CardViewWithIcon
                 style={{
                  backgroundColor: '#ffffff',
                  padding: 5,
                  margin:10,
                  borderRadius:3,
                  shadowColor: '#000000',
                  shadowOpacity: 0.3,
                  shadowRadius:3,
                  bgColor:'#ffffff',
                  elevation:3,
                  shadowOffsetWidth: 3,
                  shadowOffsetHeight: 3
                }}
                androidIcon={ 'md-chatboxes' }
                iosIcon={ 'ios-jet-outline' }
                iconBgColor={ '#4876FF' }
               // onPress={() => this.props.navigation.navigate("Chat")}
                iconColor={ '#FFFFFF' }
                title={ 'Chat' }
               // content={'27'}
              />

            </View>
        </View>

        <View style={{flex:2,flexDirection:"row",justifyContent:'space-between'}}>
            <View style={{flex:1}}>
            <CardViewWithIcon
                style={{
                  backgroundColor: '#ffffff',
                  padding: 5,
                  margin:10,
                  borderRadius:3,
                  shadowColor: '#000000',
                  shadowOpacity: 0.3,
                  shadowRadius:3,
                  bgColor:'#ffffff',
                  elevation:3,
                  shadowOffsetWidth: 3,
                  shadowOffsetHeight: 3
                }}
                androidIcon={ 'md-people' }
                iosIcon={ 'ios-jet-outline' }
                iconBgColor={ '#4876FF' }
                onPress={() => this.props.navigation.navigate("Issues")}
                iconColor={ '#FFFFFF' }
                title={ 'Issues' }
               // content={'13'}
              />

            </View>

            <View style={{flex:1}}>
            <CardViewWithIcon
                style={{
                  backgroundColor: '#ffffff',
                  padding: 5,
                  margin:10,
                  borderRadius:3,
                  shadowColor: '#000000',
                  shadowOpacity: 0.3,
                  shadowRadius:3,
                  bgColor:'#ffffff',
                  elevation:3,
                  shadowOffsetWidth: 3,
                  shadowOffsetHeight: 3
                }}
                androidIcon={ 'md-alert' }
                iosIcon={ 'ios-jet-outline' }
                iconBgColor={ '#4876FF' }
                onPress={() => this.props.navigation.navigate("Report")}
                iconColor={ '#FFFFFF' }
                title={ 'Report Crime' }
               
              />
            </View>

        </View>
</Container>   
    );
  }
}

const styles = StyleSheet.create({
  
  titleWrapper: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
  }

});